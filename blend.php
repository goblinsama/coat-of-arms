<?php
/*	Coat of Arms
	https://bitbucket.org/goblinsama/coat-of-arms/
	
	© 2021 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2006-2011 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

require_once 'graphics.php';

// Base config
{
	$av_size = array(
		'big',
		'small',
	);
	
	$av_model = array(
		'circle',
		'ellisseciccia',
		'gagliardetto',
		'ham',
		'isoscele',
		'nick1f',
		'nick2d',
		'nick4a',
		'nick6d',
		'nick7c',
		'scudo',
		'square',
	);
	
	$av_texture = array(
		'2stripesH',
		'2stripesV',
		'3stripesH',
		'3stripesV',
		'4stripesH',
		'4stripesV',
		'5stripesH',
		'5stripesV',
		'7stripesH',
		'7stripesV',
		'8stripesH',
		'8stripesV',
		'9stripesH',
		'9stripesV',
		'backslash1',
		'checquered2',
		'checquered3',
		'checquered4',
		'checquered6',
		'checquered8',
		'csk_left',
		'csk_right',
		'double_backslash',
		'double_slash',
		'none',
		'rscaglione_alzato',
		'rscaglione_basso',
		'rscaglione_doppio',
		'rscaglione_doppio_medio',
		'rscaglione_medio',
		'scaglione_alzato',
		'scaglione_basso',
		'scaglione_doppio',
		'scaglione_doppio_medio',
		'scaglione_medio',
		'simplebackslash',
		'simpleslash',
		'slash1',
		'tartan1',
	);
}

// Options
{
	$pi = $_SERVER["PATH_INFO"];
	if ( $pi[0]=='/' ) $pi = substr($pi,1); // Removes the leading /, if it's there.
	
	$token_char = '§';
	$pia = explode($token_char,$pi);
	
	$ii=0;
	$size_i=(int)$pia[$ii++];
	$opt=$pia[$ii++];
	$model_i=(int)$pia[$ii++];
	$texture_i=(int)$pia[$ii++];
	$colors=explode('.',$pia[$ii++],2)[0]; // Removes the extension at the end, if it's there.
}

// Init
{
	$size = ($av_size[$size_i] ?? NULL) or die("Couldn't find size [$size_i].");
	$model_name = ($av_model[$model_i] ?? NULL) or die("Couldn't find model [$model_i].");
	$texture_name = ($av_texture[$texture_i] ?? NULL) or die("Couldn't find texture [$texture_i],");
	
	if (strpbrk($opt,'s')) $STRETCH=FALSE; else $STRETCH=TRUE;
	
	// Colours
	
	$c1=$c2=$c3=array('r'=>0,'g'=>0,'b'=>0);
	
	$ii=0;
	$c1['r']=hexdec(substr($colors,$ii++,1))*16;
	$c1['g']=hexdec(substr($colors,$ii++,1))*16;
	$c1['b']=hexdec(substr($colors,$ii++,1))*16;
	$c2['r']=hexdec(substr($colors,$ii++,1))*16;
	$c2['g']=hexdec(substr($colors,$ii++,1))*16;
	$c2['b']=hexdec(substr($colors,$ii++,1))*16;
	$c3['r']=hexdec(substr($colors,$ii++,1))*16;
	$c3['g']=hexdec(substr($colors,$ii++,1))*16;
	$c3['b']=hexdec(substr($colors,$ii++,1))*16;
}

// Loading model and texture
{
	$model_img=imagecreatefrompng("models/$size/$model_name.png") or die("error loading model $model_name");
	$w=imagesx($model_img);
	$h=imagesy($model_img);
	$texture_img=NULL;
	$max=max($w,$h);
	@$texture_img=imagecreatefrompng("textures/$max/$texture_name.png"); // prova a prendere la texture della dimensione giusta
	if (!$texture_img) {
		@$texture_img=imagecreatefrompng("textures/0/$texture_name.png") or die("error loading texture $texture_name");
	}
}

// Setting texture colours
{
	imagecolorset($texture_img,0,$c1['r'],$c1['g'],$c1['b']);
	imagecolorset($texture_img,1,$c2['r'],$c2['g'],$c2['b']);
	imagecolorset($texture_img,2,$c3['r'],$c3['g'],$c3['b']);
}

// Stretch
if ($STRETCH) {
	$dw=$dh=max($w,$h);
	$new_model=imagecreate($dw,$dh);
	imagepalettecopy($new_model,$model_img);
	imagefill($new_model,0,0,1);
	imagecopy($new_model,$model_img,floor(($dw-$w)/2),floor(($dh-$h)/2),0,0,$w,$h);
	$model_img=$new_model;
}
else {
	$dw=$w;
	$dh=$h;
}

// Creating new image, blitting texture on it
$dest=imagecreate($dw,$dh);
imageblitall($dest,$texture_img);

// Blitting model on the destination
imagecolortransparent($model_img,0);
imageblitall($dest,$model_img);

// Setting alpha
$trgb=imagecolorsforindex($model_img,1);
$tid=imagecolorresolve($dest,$trgb["red"],$trgb["green"],$trgb["blue"]);
imagecolortransparent($dest,$tid);

header("Content-type: image/png");
imagepng($dest);
