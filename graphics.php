<?php
/*	Coat of Arms
	https://bitbucket.org/goblinsama/coat-of-arms/
	
	© 2021 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2006-2011 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

////// FUNZIONI GRAFICHE

function draw_dotx ($w, $h, $r, $g, $b, $a) {
	$w>0 or $w=32;
	$h>0 or $h=32;
	
	// immagine di destinazione
	($dest=imagecreatetruecolor($w,$h)) or die("Error dest");
	imagealphablending($dest,FALSE) or die("Sblend");
	
	imagesavealpha($dest,TRUE) or die("Canalfo");
	$color=imagecolorallocatealpha($dest, $r, $g, $b, $a); // NOTA: dunque default è nero
	imagefilledrectangle($dest, 0, 0, $w-1, $h-1, $color);
	
	// shows image
	header("Content-type: image/png");
	imagepng($dest);
	
	imagedestroy($dest);
}

// pastes src image stretched over dest image
function imageblitall (&$dest, &$src) {
	$dw=imagesx($dest);
	$dh=imagesy($dest);
	$sw=imagesx($src);
	$sh=imagesy($src);
	return imagecopyresampled($dest,$src,0,0,0,0,$dw,$dh,$sw,$sh);
}

////// ALTRE FUNZIONI

function fixhttp (&$string) {
	// TODO…… potrebbe esser fatto meglio ma sticazzi
	
	$slash=strlen('http:/');
	$split=str_split($string);
	if ($split[$slash-1]!='/') return FALSE;
	if ($split[$slash]=='/') return FALSE;
	
	$new='http://'.substr($string,$slash);
	$string=$new;
	return TRUE;
}
