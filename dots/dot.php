<?php
/*	Coat of Arms
	https://bitbucket.org/goblinsama/coat-of-arms/
	
	© 2021 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2006-2011 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

require_once '../graphics.php';

if (isset($_SERVER["PATH_INFO"])) {
	// inizializza
	$x['w']=0;
	$x['h']=0;
	$x['r']=0;
	$x['g']=0;
	$x['b']=0;
	$x['a']=0;
	
	$pi=str_split($_SERVER["PATH_INFO"]);
	$cur=NULL;
	// continua finché non trova un punto o finisce i caratteri
	for ($cmd=current($pi); $cmd!==FALSE && $cmd!='.'; $cmd=next($pi)) {
		if (!is_numeric($cmd)) {
			// nuova variabile da settare
			if (!isset($x[$cmd])) continue; // invalida
			$cur=$cmd;
			continue;
		}
		if ($cur===NULL) continue; // invalido
		$x[$cur]=$x[$cur]*10+$cmd;
	}
}
else {
	$x['w']=(int)$_GET['w'];
	$x['h']=(int)$_GET['h'];
	$x['r']=(int)$_GET['r'];
	$x['g']=(int)$_GET['g'];
	$x['b']=(int)$_GET['b'];
	$x['a']=(int)$_GET['a'];
}

draw_dotx($x['w'],$x['h'],$x['r'],$x['g'],$x['b'],$x['a']);
