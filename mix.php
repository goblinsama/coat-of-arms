<?php
/*	Coat of Arms
	https://bitbucket.org/goblinsama/coat-of-arms/
	
	© 2018-2021 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2006-2011 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

require_once 'graphics.php';

if (isset($_SERVER["PATH_INFO"])) {
	$pi=$_SERVER["PATH_INFO"];
	strtox($pi,'/');
	$colors=strtox($pi,'/');
	$bg=$pi;
}
else {
	$w=(int)$_GET["w"];
	$h=(int)$_GET["h"];
	$bg=$_GET["back"];
	$ov=$_GET["pixl"];
	$colors=$_GET["colors"];
}

if ($colors) {
	$x['r']=0;
	$x['g']=0;
	$x['b']=0;
	$x['a']=0;
	
	$pi=str_split($colors);
	$cur=NULL;
	// continua finché non trova un punto o finisce i caratteri
	for ($cmd=current($pi); $cmd!==FALSE && $cmd!='.'; $cmd=next($pi)) {
		if (!is_numeric($cmd)) {
			// nuova variabile da settare
			if (!isset($x[$cmd])) continue; // invalida
			$cur=$cmd;
			continue;
		}
		if ($cur===NULL) continue; // invalido
		$x[$cur]=$x[$cur]*10+$cmd;
	}
}
else {
	$x['r']=(int)$_GET['r'];
	$x['g']=(int)$_GET['g'];
	$x['b']=(int)$_GET['b'];
	$x['a']=(int)$_GET['a'];
}

!empty($w) or $w=1;
!empty($h) or $h=1;
$PIXEL=FALSE;

// immagine di destinazione
if ($bg) {
	$dest=imagecreatefrompng($bg) or die("Can't load");
	imagealphablending($dest,TRUE) or die("Sblend");
	if (!empty($ov)) {
		$PIXEL=TRUE;
		// TODO°° se il file indicato ha un path relativo ed è un php ?get, non funziona
		$pixl=imagecreatefrompng($ov) or die("Can't load pixel");
		imageblitall($dest,$pixl);
	}
	else {
		$w=imagesx($dest);
		$h=imagesy($dest);
	}
}
else {
	($dest=imagecreatetruecolor($w,$h)) or die("Error dest");
	imagealphablending($dest,FALSE) or die("Sblend");
}

if (!$PIXEL) {
	imagesavealpha($dest,TRUE) or die("Canalfo");
	$color=imagecolorallocatealpha($dest, $x['r'], $x['g'], $x['b'], $x['a']); // NOTA: dunque default è nero
	imagefilledrectangle($dest, 0, 0, $w-1, $h-1, $color);
}

// shows image
header("Content-type: image/png");
imagepng($dest);

imagedestroy($dest);
