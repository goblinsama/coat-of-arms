# Coat of Arms

This small library aims mostly at showing customisable coats of arms.

It seems to do other things too.

I've resurrected this from an ancient grave, and I haven't had a proper look at it yet.

## WARNING

**This is old code, yet to be inspected and scrutinised.**

**DO NOT USE IN PRODUCTION!**

Use at your own risk.

## Dependencies

It requires PHP to be compiled with [GD support](https://www.php.net/manual/en/book.image.php).

## Credits

- Code by Lohoris
- Some shield shapes are derived from Nick Staroba's
