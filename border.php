<?php
/*	Coat of Arms
	https://bitbucket.org/goblinsama/coat-of-arms/
	
	© 2021 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2006-2011 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

require_once 'graphics.php';

$DIFF=TRUE;
if (isset($_SERVER["PATH_INFO"])) {
	$pi=$_SERVER["PATH_INFO"];
	strtox($pi,'/');
	$opt=strtox($pi,'/');
	if (strpbrk($opt,"_")) {
		if (strpbrk($opt,"e")) {
			$DIFF=FALSE;
		}
		$colors=strtox($pi,'/');
	}
	else {
		$colors=$opt;
	}
	$ima=$pi;
	
	// inizializza
	$x['r']=0;
	$x['g']=0;
	$x['b']=0;
	$x['R']=0;
	$x['G']=0;
	$x['B']=0;
	
	$pi=str_split($colors);
	$cur=NULL;
	// continua finché non trova un punto o finisce i caratteri
	for ($cmd=current($pi); $cmd!==FALSE && $cmd!='.'; $cmd=next($pi)) {
		if (!is_numeric($cmd)) {
			// nuova variabile da settare
			if (!isset($x[$cmd])) continue; // invalida
			$cur=$cmd;
			continue;
		}
		if ($cur===NULL) continue; // invalido
		$x[$cur]=$x[$cur]*10+$cmd;
	}
}
else {
	$x['r']=(int)$_GET['R1'];
	$x['g']=(int)$_GET['G1'];
	$x['b']=(int)$_GET['B1'];
	$x['R']=(int)$_GET['R2'];
	$x['G']=(int)$_GET['G2'];
	$x['B']=(int)$_GET['B2'];
	$DIFF=!isset($_GET["exact"]);
	$ima=$_GET["image"];
}

$tso=20;
if (
	$DIFF
	&&
	(abs($x['r']-$x['R'])<$tso)
	&&
	(abs($x['g']-$x['G'])<$tso)
	&&
	(abs($x['b']-$x['B'])<$tso)
) {
	if ($x['r']<$x['R']) {
		$x['r']-=ceil($tso/2);
		$x['R']+=floor($tso/2);
	}
	else {
		$x['r']+=floor($tso/2);
		$x['R']-=ceil($tso/2);
	}
	if ($x['g']<$x['G']) {
		$x['g']-=ceil($tso/2);
		$x['G']+=floor($tso/2);
	}
	else {
		$x['g']+=floor($tso/2);
		$x['G']-=ceil($tso/2);
	}
	if ($x['b']<$x['B']) {
		$x['b']-=ceil($tso/2);
		$x['B']+=floor($tso/2);
	}
	else {
		$x['b']+=floor($tso/2);
		$x['B']-=ceil($tso/2);
	}
	
	foreach ($x as $xi => $xv) {
		if ($x[$xi]<0) $x[$xi]=0;
		if ($x[$xi]>255) $x[$xi]=255;
	}
}

fixhttp($ima);
$img=imagecreatefrompng($ima) or die("error loading image $ima");

imagecolorset($img,1,$x['r'],$x['g'],$x['b']);
imagecolorset($img,2,$x['R'],$x['G'],$x['B']);

header("Content-type: image/png");
imagepng($img);
