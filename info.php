<?php
/*	Coat of Arms
	https://bitbucket.org/goblinsama/coat-of-arms/
	
	© 2021 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

require_once 'graphics.php';

echo '<head><style>html { background:hsl(115, 27%, 87%); font-size:1.2em; }</style></head>';
echo '<pre>'.print_r(gd_info(),TRUE).'</pre>';
